import React, { Component } from "react";
import ReactDOM from "react-dom";
import Task from './Task';
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Modal from 'react-bootstrap/Modal'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'


class DashBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allTasks: [
        {
          taskId: 1,
          title: 'Core feature',
          name: 'Implement automatic integration',
          isTaskCompleted: false
        },
        {
          taskId: 2,
          title: 'Task 2',
          name: 'REST API implementation',
          isTaskCompleted: false
        },
        {
          taskId: 3,
          title: 'Task 3',
          name: 'DB migration',
          isTaskCompleted: false
        }
      ],
      newItem: {
        title:'',
        name:''
      }
    };
    this.deleteTask = this.deleteTask.bind(this);
    this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.addTitile = this.addTitile.bind(this);
    this.addDescription = this.addDescription.bind(this);
    // this.clearVal = this.clearVal.bind(this);
  }

  deleteTask(id) {
      var all = this.state.allTasks
      var removeIndex = all.map(function(item) { return item.taskId; }).indexOf(id);
      all.splice(removeIndex, 1);
      this.setState({ allTasks: all})  
  }

  handleClose(props) {
    this.setState({ show: false });
	}

  handleSave(props) {
    const newTask = {
      isTaskCompleted: false,
      taskId: this.state.allTasks.length + 1,
      title: this.state.newItem.title,
      name: this.state.newItem.name
    }
    var updatedAllTask = this.state.allTasks.concat(newTask)
    // console.log(" newTask.title=", newTask.title, " props=", props.newItem.title, " updatedAllTask= ", updatedAllTask)
    if((newTask.name.length==0 && newTask.title.length==0) || (newTask.name === undefined || newTask.title === undefined)) {
      // this.deleteTask(newTask.taskId)
      alert("Fields empty!!");
      this.setState({ show: false })
    } else if(newTask.name || newTask.title){
      let clearNewItem = {
        name: '',
        titile: ''
      }
      this.setState({ 
        show: false,
        allTasks: updatedAllTask,
        newItem: clearNewItem
      });
    }
    // this.clearVal();
  }

  // clearVal() {
  //   doument.getElementById('taskTitle') = '';
  //   doument.getElementById('taskDescription')= ''

  // }

	handleShow() {
		this.setState({ show: true });
	}

  addTitile(val) {
    val.preventDefault();
    var stateCopy = Object.assign({}, this.state);
    stateCopy.newItem.title = val.target.value;
    this.setState(stateCopy);
    
  }

  addDescription(val) {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.newItem.name = val.target.value;
    this.setState(stateCopy);
    // val.preventDefault();
  }

  render() {
    return (
      <Container className="mt-5">
        <Row className="pr-4">
          <Button variant="outline-dark" size="lg" className="ml-3 rounded-0 col-sm-8" 
              block onClick={this.handleShow}>+ Add Task</Button>
        </Row>
        <Row>
          {this.state.allTasks.map((item) => {
            return <Task id={item.taskId} taskName={item.title} taskTitle={item.name} 
                taskStatus={item.isTaskCompleted} parentDeleteTask={()=>{this.deleteTask(item.taskId)}}/>
          }
        )}
        </Row>
        <Modal show={this.state.show} onHide={this.handleClose}>
					<Modal.Header closeButton>
						<Modal.Title>Task Details</Modal.Title>
					</Modal.Header>
					<Modal.Body>
              <div className="form-group">
                <input type="text" className="form-control-plaintext border" placeholder="Task Titile" 
                    name="taskTitle" onChange={this.addTitile}/>
              </div>
              <div className="form-group">
                <textarea type="text" className="form-control-plaintext border" placeholder="Task Description" 
                    name="taskDescription" onChange={this.addDescription}/>
              </div>
          </Modal.Body>
					<Modal.Footer>
						<Button variant="dark" onClick={()=>{this.handleSave(this.state)}}> Add </Button>
					</Modal.Footer>
				</Modal>
      </Container>
    );
  }
}

export default DashBoard;

const wrapper = document.getElementById("root");
wrapper ? ReactDOM.render(<DashBoard />, wrapper) : false;