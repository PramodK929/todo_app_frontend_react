import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import deleteLogo from '../../images/delete2.svg'
import Image from 'react-bootstrap/Image'

class Task extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            taskStatus: false
        }
        this.taskComplete = this.taskComplete.bind(this);
        this.del = this.del.bind(this);
    }
    taskComplete (props) {
        console.log("taskComplete= ",this.props)
        this.setState({taskStatus: !this.state.taskStatus})
    }
    del (props) {
        console.log("Child props=", this.props.parentDeleteTask);
    }
    render() {
        const completed = {
            textDecoration: this.state.taskStatus ? "line-through" : "none",
            textDecorationStyle: this.state.taskStatus ? "10px solid #ddd": "0%"
        }
        return(
            <Container>
                    <div className="justify-content-center border col-sm-8 flex">
                        <Row>
                            <input type="checkbox" className="m-4" onClick={this.taskComplete}/>
                            <Card className="p-3 bg-white ml-1 col-lg-10 border-0"> 
                                <Card.Title style={completed}>{this.props.taskName}</Card.Title>
                                <Card.Text className="cardText" style={completed}>
                                    {this.props.taskTitle}
                                </Card.Text>
                            </Card>
                            <Image src={deleteLogo} style={{width: "30px", height:"30px"}} className="mt-4 ml-3 h-100 rotate" 
                                onClick= {()=> {this.props.parentDeleteTask(this.state, this.props)}}/>
                                {/* <Image src={deleteLogo} style={{width: "30px", height:"30px"}} className="mt-4 ml-3 h-100 rotate" 
                                onClick= {()=> {this.props.deleteTask(this, this.props)}}/> */}
                        </Row>
                    </div>
            </Container>
        )
    }
}

export default Task;